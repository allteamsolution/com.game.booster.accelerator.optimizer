package com.nedash.booster.accelerator.optimizer.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.nedash.booster.accelerator.optimizer.BuildConfig

@Database(entities = [AppEntity::class, ModeEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    companion object {
        private var INSTANCE: AppDatabase? = null

        fun provideDatabase(context: Context): AppDatabase {
            if (INSTANCE == null)
                INSTANCE = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    BuildConfig.APPLICATION_ID
                ).fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build()
            return INSTANCE as AppDatabase
        }
    }

    abstract fun appDao(): AppDao
    abstract fun modeDao(): ModeDao
}