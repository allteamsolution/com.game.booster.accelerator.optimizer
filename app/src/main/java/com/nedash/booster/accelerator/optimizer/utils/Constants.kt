package com.nedash.booster.accelerator.optimizer.utils

object Constants {
    const val SITE_URL = "https://kuton.icu/game_booster_accelerator_optimizer/"
    const val PP_URL = "https://kuton.icu/game_booster_accelerator_optimizer/PrivacyPolicy.php"
    const val TOU_URL = "https://kuton.icu/game_booster_accelerator_optimizer/terms.php"
    const val OUR_APPS_URL = "https://play.google.com/store/search?q=pub%3ATerraps%20group&c=apps"
    const val PURCHASE_CANCEL_SITE = "https://support.google.com/googlenews/answer/7018481?co=GENIE.Platform%3DAndroid&hl=en"

    const val SUBS_YEAR_BEFORE = "subscribe_game_booster_accelerator_optimizer_year_trial_before"
    const val SUBS_YEAR_AFTER = "subscribe_game_booster_accelerator_optimizer_year_trial_after"
    const val SUBS_OFFER = "subscribe_game_booster_accelerator_optimizer_offer"
    const val SUBS_WEEK = "subscribe_game_booster_accelerator_optimizer_week"
    const val SUBS_MONTH = "subscribe_game_booster_accelerator_optimizer_month"
    const val SUBS_YEAR = "subscribe_game_booster_accelerator_optimizer_year"
}