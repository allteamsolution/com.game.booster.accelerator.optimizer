package com.nedash.booster.accelerator.optimizer.db

import android.content.Context
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nedash.booster.accelerator.optimizer.R
import kotlinx.parcelize.Parcelize

@Entity
@Parcelize
data class ModeEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var name: String = "Default",
    var mode: Int = 1,
    var bluetooth: Boolean = false,
    var brightness: Int = 0,
    var screenRotation: Boolean = false,
    var autoSync: Boolean = false,
    var sound: Int = 0
): Parcelable {
    fun getIcon() = when (mode) {
        1 -> R.drawable.ic_no_offline_play
        2 -> R.drawable.ic_power_play
        3 -> R.drawable.ic_long_play
        4 -> R.drawable.ic_boost_play
        5 -> R.drawable.ic_water_drop_play
        6 -> R.drawable.ic_light_play
        7 -> R.drawable.ic_alarm_off_play
        else -> R.drawable.ic_no_offline_play
    }

    fun getBrightness(context: Context) = when (brightness) {
        50, 100 -> context.getString(R.string.percent, brightness)
        else -> context.getString(R.string.auto)
    }

    fun getSound(context: Context) = when (sound) {
        0 -> context.getString(R.string.off)
        1 -> context.getString(R.string.on)
        else -> context.getString(R.string.vibrate)
    }
}
