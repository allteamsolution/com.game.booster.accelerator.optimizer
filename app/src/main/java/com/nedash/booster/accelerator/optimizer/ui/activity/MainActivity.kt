package com.nedash.booster.accelerator.optimizer.ui.activity

import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.ads.*
import com.google.android.play.core.review.ReviewManagerFactory
import com.nedash.booster.accelerator.optimizer.BuildConfig
import com.nedash.booster.accelerator.optimizer.R
import com.nedash.booster.accelerator.optimizer.billing.BillingHelper
import com.nedash.booster.accelerator.optimizer.databinding.ActivityMainBinding
import com.nedash.booster.accelerator.optimizer.utils.Constants.OUR_APPS_URL
import com.nedash.booster.accelerator.optimizer.utils.Utils.gone
import com.nedash.booster.accelerator.optimizer.utils.Utils.navToInside
import com.nedash.booster.accelerator.optimizer.utils.Utils.navigateWithSlideAnimation
import com.nedash.booster.accelerator.optimizer.utils.Utils.openURL
import com.nedash.booster.accelerator.optimizer.utils.Utils.shareApp
import com.nedash.booster.accelerator.optimizer.utils.Utils.visible

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private lateinit var adView: AdView
    lateinit var billing: BillingHelper
    var isPro = false

    private val adSize: AdSize
        get() {
            val display = windowManager.defaultDisplay
            val outMetrics = DisplayMetrics()
            display.getMetrics(outMetrics)

            val density = outMetrics.density

            var adWidthPixels = binding.appBarMain.adViewContainer.width.toFloat()
            if (adWidthPixels == 0f) {
                adWidthPixels = outMetrics.widthPixels.toFloat()
            }

            val adWidth = (adWidthPixels / density).toInt()
            return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        with(binding) {
            setSupportActionBar(appBarMain.toolbar)
            setContentView(root)


            val navController = findNavController(R.id.nav_host_fragment_content_main)
            // Passing each menu ID as a set of Ids because each
            // menu should be considered as top level destinations.
            appBarConfiguration = AppBarConfiguration(
                setOf(R.id.homeFragment), drawerLayout
            )
            setupActionBarWithNavController(navController, appBarConfiguration)
            navView.setupWithNavController(navController)
            appBarMain.llGetPro.setOnClickListener { findNavController(R.id.nav_host_fragment_content_main).navToInside() }

            llPro.setOnClickListener {
                drawerLayout.close()
                findNavController(R.id.nav_host_fragment_content_main).navToInside()
            }
            llModes.setOnClickListener {
                drawerLayout.close()
                findNavController(R.id.nav_host_fragment_content_main)
                    .navigateWithSlideAnimation(R.id.modesFragment)
            }
            llSettings.setOnClickListener {
                drawerLayout.close()
                findNavController(R.id.nav_host_fragment_content_main)
                    .navigateWithSlideAnimation(R.id.settingsFragment)
            }
            llRate.setOnClickListener {
                val reviewManager = ReviewManagerFactory.create(this@MainActivity)
                val requestReviewFlow = reviewManager.requestReviewFlow()
                requestReviewFlow.addOnCompleteListener {
                    if (it.isSuccessful)
                        reviewManager.launchReviewFlow(this@MainActivity, it.result)
                }
            }
            llOurApps.setOnClickListener { openURL(OUR_APPS_URL) }
            llShare.setOnClickListener { shareApp() }
            appBarMain.ivGetProBanner.setOnClickListener {
                findNavController(R.id.nav_host_fragment_content_main).navToInside()
            }
        }
        billing = BillingHelper(this)
        with(billing) {
            init()
            isPro.observe(this@MainActivity) {
                this@MainActivity.isPro = it ?: false
                with(binding) {
                    if (this@MainActivity.isPro) {
                        with(appBarMain) {
                            llGetPro.gone()
                            rlBanner.gone()
                        }
                        llPro.gone()
                    } else {
                        with(appBarMain) {
                            llGetPro.visible()
                            rlBanner.visible()
                        }
                        llPro.visible()
                    }
                }
            }
            skuQueryError.observe(this@MainActivity) {
                it?.getContentIfNotHandled().let { error ->
                    error?.let {
                        Toast.makeText(
                            this@MainActivity,
                            R.string.some_error_occurred,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }
        showBannerOrPro()
    }

    private val listener =
        NavController.OnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.beforeTutorialFragment -> {
                    showOrHide(false, R.color.color_0088EA)
                }
                R.id.insideFragment -> {
                    showOrHide(false, R.color.color_04604B)
                }
                R.id.offerFragment -> {
                    showOrHide(false, R.color.color_FFA133)
                }
                R.id.splashFragment, R.id.afterTutorialFragment, R.id.onBoardingFragment -> {
                    showOrHide(false, R.color.black)
                }
                R.id.homeFragment -> {
                    showOrHide(true, R.color.black, false)
                }
                R.id.settingsFragment -> {
                    showOrHide(true, R.color.black, lock = false, isLlGetPro = false)
                }
                else -> {
                    showOrHide(true, R.color.black)
                }
            }
        }

    private fun showOrHide(show: Boolean, @ColorRes color: Int, lock: Boolean = true, isLlGetPro: Boolean = true) {
        window?.statusBarColor = ContextCompat.getColor(this, color)
        with(binding) {
            with(appBarMain) {
                if (show) {
                    toolbar.visible()
                    if (!isPro){
                        rlBanner.visible()
                        toolbar.visible()
                        if(!isLlGetPro) {
                            appBarMain.llGetPro.gone()
                        } else{
                            appBarMain.llGetPro.visible()
                        }
                    }
                } else {
                    toolbar.gone()
                    rlBanner.gone()
                }
                drawerLayout.setDrawerLockMode(if (lock) DrawerLayout.LOCK_MODE_LOCKED_CLOSED else DrawerLayout.LOCK_MODE_UNLOCKED)
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onResume() {
        super.onResume()
        findNavController(R.id.nav_host_fragment_content_main).addOnDestinationChangedListener(
            listener
        )
    }

    override fun onPause() {
        super.onPause()
        findNavController(R.id.nav_host_fragment_content_main).removeOnDestinationChangedListener(
            listener
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        billing.onDestroy()
    }

    override fun onBackPressed() {
        binding.appBarMain.ivGetProBanner
        if (binding.drawerLayout.isOpen)
            binding.drawerLayout.close()
        else
            when (findNavController(R.id.nav_host_fragment_content_main).currentDestination?.id) {
                R.id.beforeTutorialFragment, R.id.afterTutorialFragment, R.id.offerFragment -> return
                else -> super.onBackPressed()
            }
    }

    private fun showBannerOrPro() {
        adView = AdView(this)
        with(binding.appBarMain) {
            adViewContainer.addView(adView)
            adView.adUnitId = BuildConfig.BANNER_ADD

            adView.adSize = adSize

            val adRequest = AdRequest.Builder().build()

            // Start loading the ad in the background.
            adView.loadAd(adRequest)
            adView.adListener = object : AdListener() {
                override fun onAdLoaded() {
                    super.onAdLoaded()
                    ivGetProBanner.gone()
                    adViewContainer.visible()
                }

                override fun onAdFailedToLoad(p0: LoadAdError) {
                    super.onAdFailedToLoad(p0)
                    adViewContainer.gone()
                    ivGetProBanner.visible()
                }
            }
        }
    }

}