package com.nedash.booster.accelerator.optimizer.ui.fragment.purchase

import android.graphics.Paint
import android.graphics.Typeface
import android.os.Bundle
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.nedash.booster.accelerator.optimizer.R
import com.nedash.booster.accelerator.optimizer.databinding.FragmentBeforeTutorialBinding
import com.nedash.booster.accelerator.optimizer.ui.activity.MainActivity
import com.nedash.booster.accelerator.optimizer.utils.Constants.PP_URL
import com.nedash.booster.accelerator.optimizer.utils.Constants.PURCHASE_CANCEL_SITE
import com.nedash.booster.accelerator.optimizer.utils.Constants.SUBS_YEAR_BEFORE
import com.nedash.booster.accelerator.optimizer.utils.Constants.TOU_URL
import com.nedash.booster.accelerator.optimizer.utils.Utils.calculateOldPrice
import com.nedash.booster.accelerator.optimizer.utils.Utils.calculatePeriodPrice
import com.nedash.booster.accelerator.optimizer.utils.Utils.convertPrice
import com.nedash.booster.accelerator.optimizer.utils.Utils.openURL
import com.nedash.booster.accelerator.optimizer.utils.Utils.visible
import io.github.armcha.autolink.AutoLinkTextView
import io.github.armcha.autolink.MODE_URL

class BeforeTutorialFragment : Fragment() {

    private lateinit var binding: FragmentBeforeTutorialBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentBeforeTutorialBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.llClose.setOnClickListener { navToOnBoarding() }
        with((requireActivity() as MainActivity).billing) {
            isPro.observe(viewLifecycleOwner, { if (it == true) navToOnBoarding() })
            querySubSku(listOf(SUBS_YEAR_BEFORE)) {
                val skuDetails = it.firstOrNull()
                skuDetails?.let { sku ->
                    requireActivity().runOnUiThread {
                        with(binding) {
                            tvWeekly.text =
                                getString(R.string.price_weekly, sku.calculatePeriodPrice(52))
                            tvAnnually.text = getString(R.string.price_annually, sku.convertPrice())
                            with(tvEveryAnnually) {
                                text = getString(R.string.price_annually, sku.calculateOldPrice(25))
                                paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                            }
                            createLinkString(
                                tvBottomHint, getString(
                                    R.string.trial_purchase_bottom_hint,
                                    sku.convertPrice(), PP_URL, TOU_URL, PURCHASE_CANCEL_SITE
                                )
                            )
                            btnGetPro.setOnClickListener {
                                launchBillingFlow(
                                    requireActivity(),
                                    sku
                                )
                            }
                            btnFreeTrial.setOnClickListener {
                                launchBillingFlow(
                                    requireActivity(),
                                    sku
                                )
                            }
                            clContent.visible()
                        }
                    }
                }
            }
        }
    }

    private fun navToOnBoarding() {
        findNavController().navigate(BeforeTutorialFragmentDirections.actionBeforeTutorialFragmentToOnBoardingFragment())
    }

    private fun createLinkString(textView: AutoLinkTextView, dataText: String) {
        with(textView) {
            addAutoLinkMode(MODE_URL)
            attachUrlProcessor { s: String ->
                when {
                    s.equals(TOU_URL, ignoreCase = true) -> {
                        return@attachUrlProcessor "Terms and Conditions"
                    }
                    s.equals(PP_URL, ignoreCase = true) -> {
                        return@attachUrlProcessor "Privacy Policy"
                    }
                    else -> {
                        return@attachUrlProcessor "Cancel"
                    }
                }
            }
            addSpan(MODE_URL, StyleSpan(Typeface.NORMAL), UnderlineSpan())
            urlModeColor = currentTextColor
            text = dataText
            onAutoLinkClick { (_, _, originalText) -> requireContext().openURL(originalText) }
        }
    }
}