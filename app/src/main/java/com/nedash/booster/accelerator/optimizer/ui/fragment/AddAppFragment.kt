package com.nedash.booster.accelerator.optimizer.ui.fragment

import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.nedash.booster.accelerator.optimizer.R
import com.nedash.booster.accelerator.optimizer.databinding.FragmentAddAppBinding
import com.nedash.booster.accelerator.optimizer.db.AppEntity
import com.nedash.booster.accelerator.optimizer.db.AppDao
import com.nedash.booster.accelerator.optimizer.db.AppDatabase
import com.nedash.booster.accelerator.optimizer.ui.adapters.AddAppAdapter
import com.nedash.booster.accelerator.optimizer.utils.SharedPrefs
import com.nedash.booster.accelerator.optimizer.utils.SortType
import com.nedash.booster.accelerator.optimizer.utils.Utils.showSortDialog
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.io.File

class AddAppFragment : Fragment() {

    private lateinit var binding: FragmentAddAppBinding
    private lateinit var appDao: AppDao

    private val appAdapter = AddAppAdapter()

    private lateinit var currentSortType: SortType

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentAddAppBinding.inflate(layoutInflater, container, false)
        appDao = AppDatabase.provideDatabase(requireContext()).appDao()
        currentSortType = SharedPrefs(requireContext()).getInt(SharedPrefs.SORT_TYPE_KEY).let{ sortType ->
            if (sortType == R.string.name || sortType == 0){
                SortType.Name
            } else {
                SortType.InstallationTime
            }
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            with(rvItems) {
                layoutManager = LinearLayoutManager(requireContext())
                appAdapter.makeAllUnselected()
                adapter = appAdapter
            }
            llSortBy.setOnClickListener {
                showSortDialog(currentSortType) {
                    currentSortType = it
                    appAdapter.changeSortType(it)
                }
            }
            llAddApps.setOnClickListener {
                lifecycleScope.launch {
                    if (appAdapter.selectedAppEntities.isNotEmpty())
                        appDao.insertAll(appAdapter.selectedAppEntities)
                }
                findNavController().popBackStack()
            }
        }
        val pm = requireActivity().packageManager
        lifecycleScope.async {
            val addedApps = appDao.getAllApps()
            val packages = pm.getInstalledApplications(PackageManager.GET_META_DATA)
                .filter { it.flags and ApplicationInfo.FLAG_SYSTEM == 0 }
                .filter { app -> !addedApps.any { it.appPackage == app.packageName } }
                .map { appInfo ->
                    val file = File(appInfo.publicSourceDir)
                    AppEntity(
                        name = appInfo.loadLabel(pm).toString(),
                        appPackage = appInfo.packageName,
                        size = file.length() / (1024 * 1024),
                        installTime = pm.getPackageInfo(appInfo.packageName, 0).firstInstallTime
                    )
                }
            appAdapter.submitList(packages.sortedBy { it.name })
        }
    }

    override fun onPause() {
        super.onPause()
        val pm = requireActivity().packageManager
        lifecycleScope.async {
            val addedApps = appDao.getAllApps()
            val packages = pm.getInstalledApplications(PackageManager.GET_META_DATA)
                .filter { it.flags and ApplicationInfo.FLAG_SYSTEM == 0 }
                .filter { app -> !addedApps.any { it.appPackage == app.packageName } }
                .map { appInfo ->
                    val file = File(appInfo.publicSourceDir)
                    AppEntity(
                        name = appInfo.loadLabel(pm).toString(),
                        appPackage = appInfo.packageName,
                        size = file.length() / (1024 * 1024),
                        installTime = pm.getPackageInfo(appInfo.packageName, 0).firstInstallTime
                    )
                }
            appAdapter.makeAllUnselected()
            appAdapter.submitList(packages.sortedBy { it.name })
        }
    }
}