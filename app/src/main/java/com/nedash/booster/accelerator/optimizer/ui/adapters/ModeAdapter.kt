package com.nedash.booster.accelerator.optimizer.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nedash.booster.accelerator.optimizer.R
import com.nedash.booster.accelerator.optimizer.databinding.ModeListItemBinding
import com.nedash.booster.accelerator.optimizer.db.ModeEntity
import com.nedash.booster.accelerator.optimizer.utils.Utils.disable
import com.nedash.booster.accelerator.optimizer.utils.Utils.enable
import com.nedash.booster.accelerator.optimizer.utils.Utils.invisible
import com.nedash.booster.accelerator.optimizer.utils.Utils.visible

class ModeAdapter(
    private val onModeClickListener: (ModeEntity) -> Unit,
    private val onChangeSelectedList: (Int) -> Unit
) : ListAdapter<ModeEntity, ModeAdapter.ModeViewHolder>(
    object : DiffUtil.ItemCallback<ModeEntity>() {
        override fun areItemsTheSame(oldItem: ModeEntity, newItem: ModeEntity) = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: ModeEntity, newItem: ModeEntity) = oldItem == newItem
    }
) {

    val selectedModesList:  ArrayList<ModeEntity> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModeViewHolder {
        return ModeViewHolder(
            ModeListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ModeViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class ModeViewHolder(private var binding: ModeListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(modeEntity: ModeEntity) {
            with(binding) {
                ivModeIcon.setImageResource(modeEntity.getIcon())
                tvModeName.text = modeEntity.name
                with(root) {
                    if (modeEntity in selectedModesList){
                        lyMain.setBackgroundResource(R.drawable.mode_selected_bg)
                        tvModeName.setTextColor(ContextCompat.getColorStateList(root.context, R.color.main_green))
                        ivModeIcon.imageTintList = ContextCompat.getColorStateList(root.context, R.color.main_green)
                    } else{
                        lyMain.setBackgroundResource(R.drawable.mode_unselected_white_bg)
                        tvModeName.setTextColor(ContextCompat.getColorStateList(root.context, R.color.white))
                        ivModeIcon.imageTintList = ContextCompat.getColorStateList(root.context, R.color.white)
                    }
                    with(cbSelected) {
                        if (selectedModesList.isEmpty()) {
                            invisible()
                            disable()
                        } else {
                            visible()
                            enable()
                            isChecked = modeEntity in selectedModesList
                        }
                        setOnClickListener {
                            if (modeEntity in selectedModesList) {
                                selectedModesList.remove(modeEntity)
                                changeSelectedState(isSelected = false)
                                if (selectedModesList.size == 0)
                                    notifyDataSetChanged()
                            } else {
                                selectedModesList.add(modeEntity)
                                changeSelectedState(isSelected = true)
                            }
                            onChangeSelectedList.invoke(selectedModesList.size)
                        }
                    }
                    setOnClickListener { onModeClickListener.invoke(modeEntity) }
                    root.setOnLongClickListener {
                        if (modeEntity !in selectedModesList) {
                            selectedModesList.add(modeEntity)
                            changeSelectedState(isSelected = true)

                            if (selectedModesList.size == 1)
                                notifyDataSetChanged()
                            else{
                                notifyItemChanged(position)
                            }
                            onChangeSelectedList.invoke(selectedModesList.size)
                        }
                        return@setOnLongClickListener true
                    }
                }
            }
        }

        private fun changeSelectedState(isSelected: Boolean = false){
            with(binding){
                if (isSelected){
                    lyMain.setBackgroundResource(R.drawable.mode_selected_bg)
                    tvModeName.setTextColor(ContextCompat.getColorStateList(root.context, R.color.main_green))
                    ivModeIcon.imageTintList = ContextCompat.getColorStateList(root.context, R.color.main_green)
                } else{
                    lyMain.setBackgroundResource(R.drawable.mode_unselected_white_bg)
                    tvModeName.setTextColor(ContextCompat.getColorStateList(root.context, R.color.white))
                    ivModeIcon.imageTintList = ContextCompat.getColorStateList(root.context, R.color.white)
                }
            }
        }
    }


    fun makeAllUnselected() {
        if (selectedModesList.isNotEmpty()) {
            selectedModesList.clear()
            onChangeSelectedList.invoke(selectedModesList.size)
            notifyDataSetChanged()
        }
    }


}