package com.nedash.booster.accelerator.optimizer.ui.fragment

import android.animation.ObjectAnimator
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import androidx.navigation.fragment.findNavController
import com.nedash.booster.accelerator.optimizer.R
import com.nedash.booster.accelerator.optimizer.databinding.FragmentSplashBinding
import com.nedash.booster.accelerator.optimizer.utils.SharedPrefs
import com.nedash.booster.accelerator.optimizer.utils.Utils.getColoredText

class SplashFragment : Fragment() {

    private lateinit var binding: FragmentSplashBinding
    private lateinit var prefs: SharedPrefs

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentSplashBinding.inflate(layoutInflater, container, false)
        prefs = SharedPrefs(requireContext())
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            tvBottomText.text = getColoredText(R.string.splash_text)
            ObjectAnimator.ofInt(ivProgress, "progress", 0, 100).apply {
                duration = 1000
                repeatCount = Animation.INFINITE
                interpolator = LinearInterpolator()
            }.start()
        }
        Handler(Looper.getMainLooper()).postDelayed({
            findNavController().navigate(
                if (prefs.getBool(SharedPrefs.TUTORIAL_SHOWED))
                    SplashFragmentDirections.actionSplashFragmentToHomeFragment()
                else SplashFragmentDirections.actionSplashFragmentToBeforeTutorialFragment()
            )
        }, 1000)/*1000 have to be*/
    }
}