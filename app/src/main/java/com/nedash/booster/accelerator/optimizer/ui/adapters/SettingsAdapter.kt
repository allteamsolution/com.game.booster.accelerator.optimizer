package com.nedash.booster.accelerator.optimizer.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nedash.booster.accelerator.optimizer.databinding.SettingsListItemBinding
import com.nedash.booster.accelerator.optimizer.utils.Setting
import com.nedash.booster.accelerator.optimizer.utils.SharedPrefs
import com.nedash.booster.accelerator.optimizer.utils.Utils.gone
import com.nedash.booster.accelerator.optimizer.utils.Utils.visible

class SettingsAdapter(
    val onItemClick: (Setting) -> Unit
) : ListAdapter<Setting, SettingsAdapter.SettingsViewHolder>(
    object : DiffUtil.ItemCallback<Setting>() {
        override fun areItemsTheSame(oldItem: Setting, newItem: Setting) =
            oldItem.title == newItem.title

        override fun areContentsTheSame(oldItem: Setting, newItem: Setting) = oldItem == newItem
    }
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingsViewHolder {
        return SettingsViewHolder(
            SettingsListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: SettingsViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class SettingsViewHolder(private val binding: SettingsListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(setting: Setting) {
            with(binding) {
                ivIcon.setImageResource(setting.icon)
                tvTitle.setText(setting.title)
                if (setting.hasSwitch) {
                    val prefs = SharedPrefs(root.context)
                    with(sw) {
                        visible()
                        isChecked = when (setting) {
                            is Setting.Notifications -> prefs.getBool(SharedPrefs.SHOW_NOTIFICATIONS)
                            else -> false
                        }
                        setOnCheckedChangeListener { _, b ->
                            if (setting == Setting.Notifications)
                                prefs.putValue(SharedPrefs.SHOW_NOTIFICATIONS, b)
                        }
                    }
                } else
                    sw.gone()
                root.setOnClickListener { onItemClick.invoke(setting) }
            }
        }
    }
}