package com.nedash.booster.accelerator.optimizer.utils

import android.app.Activity
import android.app.Dialog
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import com.android.billingclient.api.SkuDetails
import com.nedash.booster.accelerator.optimizer.BuildConfig
import com.nedash.booster.accelerator.optimizer.R
import com.nedash.booster.accelerator.optimizer.databinding.DialogPermissionBinding
import com.nedash.booster.accelerator.optimizer.databinding.DialogSortAppsBinding
import com.nedash.booster.accelerator.optimizer.utils.SharedPrefs.Companion.SORT_TYPE_KEY
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

object Utils {

    fun View.gone() {
        visibility = View.GONE
    }

    fun View.visible() {
        visibility = View.VISIBLE
    }

    fun View.invisible() {
        visibility = View.INVISIBLE
    }

    fun View.enable() {
        isEnabled = true
    }

    fun View.disable() {
        isEnabled = false
    }

    fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    fun NavController.navigateWithSlideAnimation(@IdRes resId: Int) {
        val navOptions = NavOptions.Builder().apply {
            setEnterAnim(R.anim.slide_in_right)
            setExitAnim(R.anim.slide_out_left)
            setPopEnterAnim(R.anim.slide_in_left)
            setPopExitAnim(R.anim.slide_out_right)
        }
        navigate(resId, null, navOptions.build())
    }

    fun NavController.navToInside() {
        val navOptions = NavOptions.Builder().apply {
            setEnterAnim(android.R.anim.fade_in)
            setExitAnim(android.R.anim.fade_out)
        }
        navigate(R.id.insideFragment, null, navOptions.build())
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun Fragment.getColoredText(@StringRes id: Int, vararg args: Any?): CharSequence =
        HtmlCompat.fromHtml(String.format(getString(id), *args), HtmlCompat.FROM_HTML_MODE_COMPACT)

    fun Fragment.showSortDialog(
        currentSortType: SortType,
        onOkClicked: (SortType) -> Unit
    ) {
        val bind = DialogSortAppsBinding.inflate(layoutInflater)
        with(Dialog(requireContext())) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setCancelable(false)
            setContentView(bind.root)
            with(bind) {
                rbName.setOnClickListener {
                    dismiss()
                    onOkClicked.invoke(SortType.Name)
                    SharedPrefs(requireContext()).putValue(SORT_TYPE_KEY, SortType.Name.name)
                }
                rbTime.setOnClickListener {
                    dismiss()
                    onOkClicked.invoke(SortType.InstallationTime)
                    SharedPrefs(requireContext()).putValue(SORT_TYPE_KEY, SortType.InstallationTime.name)

                }
                ivClose.setOnClickListener { dismiss() }
            }
            show()
        }
    }

    fun Context.checkPermissions(): Boolean {
        val n =
            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        return if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M)
            Settings.System.canWrite(this) && n.isNotificationPolicyAccessGranted
        else true
    }

    fun Fragment.showPermissionDialog() = showPermissionDialog(requireContext(), layoutInflater)

    private fun showPermissionDialog(
        context: Context,
        layoutInflater: LayoutInflater
    ): Dialog {
        val bind = DialogPermissionBinding.inflate(layoutInflater)
        with(Dialog(context)) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setContentView(bind.root)
            with(bind) {
                ivClose.setOnClickListener { dismiss() }
                btnOk.setOnClickListener {
                    dismiss()
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        if (!Settings.System.canWrite(context))
                            context.startActivity(Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS).apply {
                                data = Uri.parse("package:" + context.packageName)
                            })
                        else
                            context.startActivity(Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS))
                    }
                }
            }
            show()
            return this
        }
    }

    fun Context.appInstalledOrNot(uri: String): Boolean {
        val pm = packageManager
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            return false
        } catch (e: PackageManager.NameNotFoundException) {
        }
        return true
    }

    fun Context.openURL(url: String) {
        try {
            startActivity(Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse(url)
            })
        } catch (e: Exception) {
            Toast.makeText(this, R.string.no_application_found, Toast.LENGTH_SHORT).show()
        }
    }

    fun Context.shareApp() {
        try {
            startActivity(Intent(Intent.ACTION_SEND).apply {
                type = "text/plain"
                putExtra(
                    Intent.EXTRA_TEXT,
                    "https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}"
                )
            })
        } catch (e: Exception) {
            Toast.makeText(this, R.string.no_application_found, Toast.LENGTH_SHORT).show()
        }
    }

    fun SkuDetails.convertPrice(): String {
        val price = priceAmountMicros / 1000000.0
        return getText(price.toString())
    }

    fun SkuDetails.calculateOldPrice(discount: Int): String {
        with(DecimalFormat("#.##")) {
            decimalFormatSymbols = DecimalFormatSymbols.getInstance(Locale.ENGLISH)
            val price =
                format(priceAmountMicros / 1000000.0 * 100 / (100 - discount))
            return getText(price)
        }
    }

    fun SkuDetails.calculatePeriodPrice(period: Int): String {
        with(DecimalFormat("#.##")) {
            decimalFormatSymbols = DecimalFormatSymbols.getInstance(Locale.ENGLISH)
            val price = format(priceAmountMicros / 1000000.0 / period)
            return getText(price)
        }
    }

    private fun SkuDetails.getText(price: String): String =
        if (priceCurrencyCode.length > 1) "$price $priceCurrencyCode" else priceCurrencyCode + price
}

open class Event<out T>(private val content: T) {

    var hasBeenHandled = false
        private set // Allow external read but not write

    /**
     * Returns the content and prevents its use again.
     */
    fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }

    /**
     * Returns the content, even if it's already been handled.
     */
    fun peekContent(): T = content
}