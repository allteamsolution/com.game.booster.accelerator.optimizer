package com.nedash.booster.accelerator.optimizer.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface AppDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertAll(appEntities: List<AppEntity>)

    @Delete
    abstract suspend fun deleteApps(list: List<AppEntity>)

    @Delete
    abstract suspend fun deleteApp(appEntity: AppEntity)

    @Update
    abstract suspend fun updateApp(appEntity: AppEntity)

    @Query("SELECT * FROM AppEntity")
    abstract fun getAll(): LiveData<List<AppEntity>>

    @Query("SELECT * FROM AppEntity")
    abstract fun getAllApps(): List<AppEntity>
}