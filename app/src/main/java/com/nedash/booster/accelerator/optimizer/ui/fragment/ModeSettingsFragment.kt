package com.nedash.booster.accelerator.optimizer.ui.fragment

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.nedash.booster.accelerator.optimizer.R
import com.nedash.booster.accelerator.optimizer.databinding.DialogBrightnessBinding
import com.nedash.booster.accelerator.optimizer.databinding.DialogSoundBinding
import com.nedash.booster.accelerator.optimizer.databinding.FragmentModeSettingsBinding
import com.nedash.booster.accelerator.optimizer.db.AppDatabase
import com.nedash.booster.accelerator.optimizer.db.ModeDao
import com.nedash.booster.accelerator.optimizer.db.ModeEntity
import com.nedash.booster.accelerator.optimizer.ui.adapters.ModeSelectorAdapter
import com.nedash.booster.accelerator.optimizer.utils.ModeSelector
import com.nedash.booster.accelerator.optimizer.utils.Utils.hideKeyboard
import kotlinx.coroutines.launch

class ModeSettingsFragment : Fragment() {

    private lateinit var binding: FragmentModeSettingsBinding
    private lateinit var modeDao: ModeDao

    private val modeSelectorAdapter = ModeSelectorAdapter()
    private val args by navArgs<ModeSettingsFragmentArgs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        modeDao = AppDatabase.provideDatabase(requireContext()).modeDao()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentModeSettingsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            with(rvItems) {
                layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                adapter = modeSelectorAdapter
            }
            modeSelectorAdapter.submitList(ModeSelector.getAllModes().map {
                it.isSelected = it.id == args.modeEntity.mode
                return@map it
            })
            with(args.modeEntity) {
                etName.setText(name)
                swBle.isChecked = bluetooth
                tvBri.text = getBrightness(requireContext())
                swSr.isChecked = screenRotation
                swAs.isChecked = autoSync
                tvSound.text = getSound(requireContext())
                clBri.setOnClickListener {
                    val bind = DialogBrightnessBinding.inflate(layoutInflater)
                    with(Dialog(requireContext())) {
                        requestWindowFeature(Window.FEATURE_NO_TITLE)
                        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        setContentView(bind.root)
                        with(bind) {
                            when (brightness) {
                                50 -> rb50.isChecked = true
                                100 -> rb100.isChecked = true
                                0 -> rbAuto.isChecked = true
                            }
                            rb50.setOnClickListener {
                                brightness = 50
                                tvBri.text = getBrightness(requireContext())
                                dismiss()
                            }
                            rb100.setOnClickListener {
                                brightness = 100
                                tvBri.text = getBrightness(requireContext())
                                dismiss()
                            }
                            rbAuto.setOnClickListener {
                                brightness = 0
                                tvBri.text = getBrightness(requireContext())
                                dismiss()
                            }
                            ivClose.setOnClickListener { dismiss() }
                        }
                        show()
                    }
                }
                clSound.setOnClickListener {
                    val bind = DialogSoundBinding.inflate(layoutInflater)
                    with(Dialog(requireContext())) {
                        requestWindowFeature(Window.FEATURE_NO_TITLE)
                        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        setContentView(bind.root)
                        with(bind) {
                            when (sound) {
                                0 -> rbOff.isChecked = true
                                1 -> rbOn.isChecked = true
                                2 -> rbVibrate.isChecked = true
                            }
                            rbOff.setOnClickListener {
                                sound = 0
                                tvSound.text = getSound(requireContext())
                                dismiss()
                            }
                            rbOn.setOnClickListener {
                                sound = 1
                                tvSound.text = getSound(requireContext())
                                dismiss()
                            }
                            rbVibrate.setOnClickListener {
                                sound = 2
                                tvSound.text = getSound(requireContext())
                                dismiss()
                            }

                            ivClose.setOnClickListener { dismiss() }
                        }
                        show()
                    }
                }
                btnSave.setOnClickListener {
                    lifecycleScope.launch {
                        with(binding) {
                            with(args.modeEntity) {
                                modeDao.insertModes(
                                    ModeEntity(
                                        id,
                                        etName.text.toString().trim().replace("\n", " "),
                                        modeSelectorAdapter.currentList.find { it.isSelected }?.id
                                            ?: 1,
                                        swBle.isChecked,
                                        brightness,
                                        swSr.isChecked,
                                        swAs.isChecked,
                                        sound
                                    )
                                )
                            }
                        }
                    }
                    findNavController().popBackStack()
                }
            }
        }
    }


    private fun setSoundValue(){

    }

    override fun onPause() {
        super.onPause()
        hideKeyboard()
    }
}