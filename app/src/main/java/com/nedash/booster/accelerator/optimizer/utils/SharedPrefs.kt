package com.nedash.booster.accelerator.optimizer.utils

import android.content.Context

class SharedPrefs(context: Context) {

    companion object {
        const val LAST_OFFER_SHOW = "last_offer_show"
        const val TUTORIAL_SHOWED = "tutorial_showed"
        const val SHOW_NOTIFICATIONS = "show_notifications"
        const val MODES_FIRST_INSERT = "modes_first_insert"
        const val OFFER_SHOW_COUNTER = "offer_show_counter"
        const val SORT_TYPE_KEY = "SORT_TYPE_KEY"
    }

    private val prefs = context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)

    fun putValue(key: String, value: Any) {
        with(prefs.edit()) {
            when (value) {
                is String -> putString(key, value)
                is Int -> putInt(key, value)
                is Long -> putLong(key, value)
                is Boolean -> putBoolean(key, value)
            }
            apply()
        }
    }

    fun getString(key: String) = prefs.getString(key, null)

    fun getInt(key: String) = prefs.getInt(key, 0)

    fun getLong(key: String) = prefs.getLong(key, 0L)

    fun getBool(key: String, default: Boolean = false) = prefs.getBoolean(key, default)
}