package com.nedash.booster.accelerator.optimizer.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nedash.booster.accelerator.optimizer.R
import com.nedash.booster.accelerator.optimizer.databinding.SelectAppListItemBinding
import com.nedash.booster.accelerator.optimizer.db.AppEntity
import com.nedash.booster.accelerator.optimizer.utils.SortType
import com.nedash.booster.accelerator.optimizer.utils.Utils.appInstalledOrNot


class AddAppAdapter : ListAdapter<AppEntity, AddAppAdapter.AddAppViewHolder>(
    object : DiffUtil.ItemCallback<AppEntity>() {
        override fun areItemsTheSame(oldItem: AppEntity, newItem: AppEntity) = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: AppEntity, newItem: AppEntity) = oldItem == newItem
    }
) {

    val selectedAppEntities: ArrayList<AppEntity> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddAppViewHolder {
        return AddAppViewHolder(
            SelectAppListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: AddAppViewHolder, position: Int) {
        currentList[position].let {
            if (holder.itemView.context.appInstalledOrNot(it.appPackage)){
                makeAllUnselected()
                return@let
            } else {
                holder.bind(currentList[position], position)
            }
        }
    }

    fun makeAllUnselected() {
        if (selectedAppEntities.isNotEmpty()) {
            selectedAppEntities.clear()
//            onChangeSelectedList.invoke(selectedAppEntities.size)
            notifyDataSetChanged()
        }
    }

    fun changeSortType(type: SortType) {
        when (type) {
            SortType.Name -> submitList(currentList.sortedBy { it.name })
            SortType.InstallationTime -> submitList(currentList.sortedBy { it.installTime })
        }
    }

    inner class AddAppViewHolder(private val binding: SelectAppListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(appEntity: AppEntity, position: Int) {
            with(binding) {
                ivAppIcon.setImageDrawable(
                    root.context.packageManager.getApplicationIcon(appEntity.appPackage)
                )
                if (appEntity in selectedAppEntities){
                    cbSelected.isChecked = true
                    clMain.setBackgroundResource(R.drawable.mode_selected_bg)
                } else{
                    cbSelected.isChecked = false
                    clMain.setBackgroundResource(R.drawable.mode_unselected_white_bg)
                }

                tvSize.text = tvSize.context.getString(R.string.size, appEntity.size)
                tvAppName.text = appEntity.name
                cbSelected.isChecked = appEntity in selectedAppEntities
                root.setOnClickListener {
                    addAppToSelectedList(appEntity)
                }
                cbSelected.setOnClickListener {
                    addAppToSelectedList(appEntity)
                }
            }
        }

        private fun addAppToSelectedList(appEntity: AppEntity){
            if (appEntity in selectedAppEntities){
                selectedAppEntities.remove(appEntity)
                changeSelectedState(false)
            }
            else{
                selectedAppEntities.add(appEntity)
                changeSelectedState(true)
            }
        }

        private fun changeSelectedState(isSelected: Boolean = false){
            with(binding){
                if (isSelected){
                    cbSelected.isChecked = true
                    clMain.setBackgroundResource(R.drawable.mode_selected_bg)
//                    tvAppName.setTextColor(ContextCompat.getColorStateList(root.context, R.color.main_green))
                } else{
                    cbSelected.isChecked = false
                    clMain.setBackgroundResource(R.drawable.mode_unselected_white_bg)
//                    tvAppName.setTextColor(ContextCompat.getColorStateList(root.context, R.color.white))
                }
            }
        }
    }
}