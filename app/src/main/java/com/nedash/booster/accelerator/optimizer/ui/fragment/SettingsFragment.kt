package com.nedash.booster.accelerator.optimizer.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.nedash.booster.accelerator.optimizer.databinding.FragmentSettingsBinding
import com.nedash.booster.accelerator.optimizer.ui.activity.MainActivity
import com.nedash.booster.accelerator.optimizer.ui.adapters.SettingsAdapter
import com.nedash.booster.accelerator.optimizer.utils.Constants.PP_URL
import com.nedash.booster.accelerator.optimizer.utils.Constants.SITE_URL
import com.nedash.booster.accelerator.optimizer.utils.Constants.TOU_URL
import com.nedash.booster.accelerator.optimizer.utils.Setting
import com.nedash.booster.accelerator.optimizer.utils.Utils.navToInside
import com.nedash.booster.accelerator.optimizer.utils.Utils.openURL

class SettingsFragment : Fragment() {

    private lateinit var binding: FragmentSettingsBinding

    private val settingsAdapter = SettingsAdapter {
        when (it) {
            Setting.AboutYourPhone -> findNavController().navigate(SettingsFragmentDirections.actionSettingsFragmentToAboutPhoneFragment())
            Setting.Notifications -> {}
            Setting.OurSite -> requireContext().openURL(SITE_URL)
            Setting.PrivacyPolicy -> requireContext().openURL(PP_URL)
            Setting.RemoveAds -> findNavController().navToInside()
            Setting.TermsOfUse -> requireContext().openURL(TOU_URL)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentSettingsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding.rvSettings) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = settingsAdapter
        }
        settingsAdapter.submitList(Setting.getAllSettings((requireActivity() as MainActivity).isPro))
    }
}