package com.nedash.booster.accelerator.optimizer.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class AppEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var name: String = "",
    var appPackage: String = "",
    var installTime: Long = 0L,
    var size: Long = 0,
    var mode_id: Int = 0
)
