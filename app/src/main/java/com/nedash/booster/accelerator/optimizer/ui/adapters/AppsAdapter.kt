package com.nedash.booster.accelerator.optimizer.ui.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nedash.booster.accelerator.optimizer.R
import com.nedash.booster.accelerator.optimizer.databinding.AppListItemBinding
import com.nedash.booster.accelerator.optimizer.db.AppEntity
import com.nedash.booster.accelerator.optimizer.utils.SortType
import com.nedash.booster.accelerator.optimizer.utils.Utils.appInstalledOrNot
import com.nedash.booster.accelerator.optimizer.utils.Utils.disable
import com.nedash.booster.accelerator.optimizer.utils.Utils.enable
import com.nedash.booster.accelerator.optimizer.utils.Utils.invisible
import com.nedash.booster.accelerator.optimizer.utils.Utils.visible

class AppsAdapter(
    private val onChangeSelectedList: (Int) -> Unit,
    private val onItemClicked: (AppEntity) -> Unit
) : ListAdapter<AppEntity, AppsAdapter.AppViewHolder>(
    object : DiffUtil.ItemCallback<AppEntity>() {
        override fun areItemsTheSame(oldItem: AppEntity, newItem: AppEntity) = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: AppEntity, newItem: AppEntity) = oldItem == newItem
    }
) {

    val selectedAppEntities: ArrayList<AppEntity> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppViewHolder {
        return AppViewHolder(
            AppListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: AppViewHolder, position: Int) {
        currentList[position].let {
            if (holder.itemView.context.appInstalledOrNot(it.appPackage)){
                makeAllUnselected()
                return@let
            } else {
                holder.bind(currentList[position], position)
            }
        }
    }

    fun makeAllSelected() {
        selectedAppEntities.clear()

        selectedAppEntities.addAll(currentList)
        onChangeSelectedList.invoke(selectedAppEntities.size)
        notifyDataSetChanged()
    }

    fun makeAllUnselected() {
        if (selectedAppEntities.isNotEmpty()) {
            selectedAppEntities.clear()
            onChangeSelectedList.invoke(selectedAppEntities.size)
            notifyDataSetChanged()
        }
    }

    fun changeSortType(type: SortType?) {
        type?.let {
            when (type) {
                SortType.Name -> submitList(currentList.sortedBy { it.name })
                SortType.InstallationTime -> submitList(currentList.sortedBy { it.installTime })
            }
        }
    }

    inner class AppViewHolder(private val binding: AppListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(appEntity: AppEntity, position: Int) {
            with(binding) {
                ivAppIcon.setImageDrawable(
                    root.context.packageManager.getApplicationIcon(
                        appEntity.appPackage
                    )
                )
                tvAppName.text = appEntity.name

                if (appEntity in selectedAppEntities){
                    linearLayout.setBackgroundResource(R.drawable.mode_selected_bg)
                } else{
                    linearLayout.setBackgroundResource(R.drawable.mode_unselected_white_bg)
                }
                with(cbSelected) {
                    if (selectedAppEntities.isEmpty()) {
                        invisible()
                        disable()
                    } else {
                        visible()
                        enable()

                        isChecked = appEntity in selectedAppEntities
                    }
                    setOnClickListener {
                        if (appEntity in selectedAppEntities) {
                            selectedAppEntities.remove(appEntity)
                            changeSelectedState(isSelected = false)
                            if (selectedAppEntities.size == 0)
                                notifyDataSetChanged()
                        } else {
                            selectedAppEntities.add(appEntity)
                            changeSelectedState(isSelected = true)
                        }
                        onChangeSelectedList.invoke(selectedAppEntities.size)
                    }
                }
                root.setOnClickListener {
                    onItemClicked.invoke(appEntity)
                    makeAllUnselected()
                }
                root.setOnLongClickListener {
                    if (appEntity !in selectedAppEntities) {
                        selectedAppEntities.add(appEntity)
                        changeSelectedState(isSelected = true)

                        if (selectedAppEntities.size == 1)
                            notifyDataSetChanged()
                        else{
                            notifyItemChanged(position)
                        }
                        onChangeSelectedList.invoke(selectedAppEntities.size)
                    }
                    return@setOnLongClickListener true
                }
            }
        }
        private fun changeSelectedState(isSelected: Boolean = false){
            with(binding){
                if (isSelected){
                    linearLayout.setBackgroundResource(R.drawable.mode_selected_bg)
//                    tvAppName.setTextColor(ContextCompat.getColorStateList(root.context, R.color.main_green))
                } else{
                    linearLayout.setBackgroundResource(R.drawable.mode_unselected_white_bg)
//                    tvAppName.setTextColor(ContextCompat.getColorStateList(root.context, R.color.white))
                }
            }
        }
    }
}