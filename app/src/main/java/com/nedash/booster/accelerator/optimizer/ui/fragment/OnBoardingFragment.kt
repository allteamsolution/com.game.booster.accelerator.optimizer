package com.nedash.booster.accelerator.optimizer.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.nedash.booster.accelerator.optimizer.R
import com.nedash.booster.accelerator.optimizer.databinding.FragmentOnBoardingBinding
import com.nedash.booster.accelerator.optimizer.databinding.FragmentOnBoardingViewPagerContentBinding
import com.nedash.booster.accelerator.optimizer.ui.activity.MainActivity

class OnBoardingFragment : Fragment() {

    private lateinit var binding: FragmentOnBoardingBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentOnBoardingBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val pagerAdapter = OnBoardingSlidePagerAdapter(this)
        with(binding) {
            introPager.adapter = pagerAdapter
            TabLayoutMediator(intoTabLayout, introPager)
            { _, _ -> }.attach()
            introPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    textViewNext.setText(
                        when (position) {
                            2 -> R.string.start
                            else -> R.string.next
                        }
                    )
                }
            })
            tvNext.setOnClickListener {
                when (introPager.currentItem) {
                    2 -> navNext()
                    else -> introPager.currentItem = introPager.currentItem + 1
                }
            }
            tvSkip.setOnClickListener { navNext() }
        }
    }

    private fun navNext() {
        findNavController().navigate(
            if ((requireActivity() as MainActivity).isPro)
                OnBoardingFragmentDirections.actionOnBoardingFragmentToHomeFragment()
            else OnBoardingFragmentDirections.actionOnBoardingFragmentToAfterTutorialFragment()
        )
    }

    inner class OnBoardingSlidePagerAdapter(
        fragment: Fragment,
    ) : FragmentStateAdapter(fragment) {

        override fun getItemCount() = 3

        override fun createFragment(position: Int): Fragment {
            return OnBoardingViewPagerContentFragment().apply {
                arguments = Bundle().apply { putInt("position", position) }
            }
        }
    }
}

class OnBoardingViewPagerContentFragment : Fragment() {

    private lateinit var binding: FragmentOnBoardingViewPagerContentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding =
            FragmentOnBoardingViewPagerContentBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            arguments?.takeIf { it.containsKey("position") }?.apply {
                when (getInt("position")) {
                    0 -> {
                        root.setBackgroundResource(R.drawable.background_tutorial_1)
                        ivIcon.setImageResource(R.drawable.tutorial_1)
                        tvHint.setText(R.string.on_boarding_1)
                    }
                    1 -> {
                        root.setBackgroundResource(R.drawable.background_tutorial_2)

                        ivIcon.setImageResource(R.drawable.img_tutorial_2)
                        tvHint.setText(R.string.on_boarding_2)
                    }
                    else -> {
                        root.setBackgroundResource(R.drawable.background_tutorial_3)

                        ivIcon.setImageResource(R.drawable.tutorial_3)
                        tvHint.setText(R.string.on_boarding_3)
                    }
                }
            }
        }
    }
}