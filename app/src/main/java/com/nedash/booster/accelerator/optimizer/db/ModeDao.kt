package com.nedash.booster.accelerator.optimizer.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ModeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertModes(vararg modeEntity: ModeEntity): List<Long>

    @Delete
    suspend fun deleteModes(list: List<ModeEntity>)

    @Query("SELECT * FROM ModeEntity")
    fun getAll(): LiveData<List<ModeEntity>>
}