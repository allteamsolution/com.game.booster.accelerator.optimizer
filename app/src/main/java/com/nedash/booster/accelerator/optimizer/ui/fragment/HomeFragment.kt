package com.nedash.booster.accelerator.optimizer.ui.fragment

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.app.Dialog
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.bluetooth.BluetoothManager
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.media.AudioManager
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.text.format.DateUtils
import android.view.*
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.nedash.booster.accelerator.optimizer.R
import com.nedash.booster.accelerator.optimizer.ads.Interstitial
import com.nedash.booster.accelerator.optimizer.databinding.*
import com.nedash.booster.accelerator.optimizer.db.*
import com.nedash.booster.accelerator.optimizer.ui.activity.MainActivity
import com.nedash.booster.accelerator.optimizer.ui.adapters.AppsAdapter
import com.nedash.booster.accelerator.optimizer.ui.adapters.SelectModeAdapter
import com.nedash.booster.accelerator.optimizer.utils.SharedPrefs
import com.nedash.booster.accelerator.optimizer.utils.SortType
import com.nedash.booster.accelerator.optimizer.utils.Utils.checkPermissions
import com.nedash.booster.accelerator.optimizer.utils.Utils.showPermissionDialog
import com.nedash.booster.accelerator.optimizer.utils.Utils.showSortDialog
import com.nedash.booster.accelerator.optimizer.utils.SharedPrefs.Companion.SORT_TYPE_KEY
import com.nedash.booster.accelerator.optimizer.utils.Utils.gone
import com.nedash.booster.accelerator.optimizer.utils.Utils.visible
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import kotlin.math.roundToInt

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var appDao: AppDao
    private lateinit var modeDao: ModeDao
    private lateinit var prefs: SharedPrefs
    private lateinit var inter: Interstitial
    private var modes = listOf<ModeEntity>()
    private var permissionDialog: Dialog? = null

    private lateinit var currentSortType: SortType

    private val appsAdapter = AppsAdapter({ updateDeleteView(it) }, { app ->
        val bind = DialogAppActionsBinding.inflate(layoutInflater)
        with(Dialog(requireContext())) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setContentView(bind.root)
            with(bind) {
                llBoost.setOnClickListener {
                    dismiss()
                    showBoostDialog(app)
                }
                llApplyMode.setOnClickListener {
                    dismiss()
                    showSelectModeDialog(app.mode_id) { modeId ->
                        lifecycleScope.launch {
                            appDao.updateApp(app.apply {
                                mode_id = modeId
                            })
                        }
                    }
                }
                llDelete.setOnClickListener {
                    lifecycleScope.launch { appDao.deleteApp(app) }
                    dismiss()
                }
                ivCLose.setOnClickListener {
                    dismiss()
                }
            }
            show()
        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        val db = AppDatabase.provideDatabase(requireContext())
        appDao = db.appDao()
        modeDao = db.modeDao()
        prefs = SharedPrefs(requireContext())
        inter = Interstitial(requireContext())
        prefs.putValue(SharedPrefs.TUTORIAL_SHOWED, true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        currentSortType = SharedPrefs(requireContext()).getInt(SORT_TYPE_KEY).let{ sortType ->
            if (sortType == R.string.name || sortType == 0){
                SortType.Name
            } else {
                SortType.InstallationTime
            }
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            with(rvItems) {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = appsAdapter
            }
            llAddApps.setOnClickListener {
                findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToAddAppFragment())
            }
            llDelete.setOnClickListener {
                lifecycleScope.launch { appDao.deleteApps(appsAdapter.selectedAppEntities) }.invokeOnCompletion {
                    appsAdapter.makeAllUnselected()
                    updateDeleteView(0)
                }

            }
            llApplyMode.setOnClickListener {
                showSelectModeDialog(0) { modeId ->
                    lifecycleScope.launch {
                        appDao.insertAll(appsAdapter.selectedAppEntities.map {
                            it.mode_id = modeId
                            return@map it
                        })
                    }.invokeOnCompletion {
                        appsAdapter.makeAllUnselected()
                        updateDeleteView(0)

                    }
                }
            }
        }
        appDao.getAll().observe(viewLifecycleOwner, { apps ->
            with(appsAdapter) {
                submitList(apps.sortedWith(
                    if (currentSortType == SortType.Name) compareBy { it.name } else compareBy { it.installTime }
                ))
//                makeAllUnselected()
            }
        })
        modeDao.getAll().observe(viewLifecycleOwner, { modes = it })
        showLimitedOffer()
    }


    private fun showSelectModeDialog(selectedMode: Int, onOkClicked: (Int) -> Unit) {
        val bind = DialogSelectModeBinding.inflate(layoutInflater)
        with(Dialog(requireContext())) {
            val modesAdapter = SelectModeAdapter(modes, selectedMode, this) { onOkClicked(it) }
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setContentView(bind.root)
            with(bind) {
                ivClose.setOnClickListener { dismiss() }
                with(rvModes) {
                    layoutManager = LinearLayoutManager(requireContext())
                    adapter = modesAdapter
                }
            }
            show()
        }
    }

    private fun showBoostDialog(appEntity: AppEntity) {
        val bind = DialogBoostBinding.inflate(layoutInflater)
        with(Dialog(requireContext())) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setContentView(bind.root)
            with(bind) {
                ivClose.setOnClickListener { dismiss() }
                val icon = requireContext().packageManager.getApplicationIcon(appEntity.appPackage)
                ivAppIcon.setImageDrawable(icon)
                val size = getString(R.string.size, appEntity.size)
                tvAppSize.text = size
                tvAppName.text = appEntity.name
                btnBoost.setOnClickListener {
                    val mode = modes.find { it.id == appEntity.mode_id }
                    if (mode != null)
                        if (requireContext().checkPermissions()) {
                            inter.showInterstitial(
                                requireActivity(),
                                (requireActivity() as MainActivity).isPro
                            ) {
                                startBoosting(appEntity, mode, icon, size)
                                dismiss()
                            }
                        } else
                            showPermissionDialog()
                    else
                        Toast.makeText(
                            requireContext(),
                            R.string.mode_not_selected,
                            Toast.LENGTH_LONG
                        ).show()
                }
            }
            show()
        }
    }

    private fun startBoosting(appEntity: AppEntity, modeEntity: ModeEntity, icon: Drawable, size: String) {
        val bind = DialogBoostingBinding.inflate(layoutInflater)
        with(Dialog(requireContext())) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setContentView(bind.root)
            setCancelable(false)
            with(bind) {
                ivAppIcon.setImageDrawable(icon)
                tvAppName.text = appEntity.name
                tvAppSize.text = size
                ObjectAnimator.ofFloat(ivLoading, View.ROTATION, 0.0f, 360.0f).apply {
                    duration = 1500
                    repeatCount = Animation.INFINITE
                    interpolator = LinearInterpolator()
                }.start()
                ValueAnimator.ofInt(0, 100).apply {
                    duration = 1500
                    addUpdateListener {
                        tvProgress.text = getString(R.string.percent, it.animatedValue as Int)
                    }
                }.start()
            }
            Handler(Looper.getMainLooper()).postDelayed({
                dismiss()
                sendNotification()
                startActivity(requireContext().packageManager.getLaunchIntentForPackage(appEntity.appPackage))
            }, 1500)
            changeSettings(modeEntity)
            show()
        }
    }

    private fun changeSettings(modeEntity: ModeEntity) {
        // Set Bluetooth
        val bluetoothManager =
            requireContext().getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        if (modeEntity.bluetooth)
            bluetoothManager.adapter.enable()
        else
            bluetoothManager.adapter.disable()
        // Set brightness
        when(modeEntity.brightness){
            0 -> {
                Settings.System.putInt(
                    requireContext().contentResolver,
                    Settings.System.SCREEN_BRIGHTNESS_MODE,
                    Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC
                )
            }
            50 -> {
                Settings.System.putInt(
                    requireContext().contentResolver,
                    Settings.System.SCREEN_BRIGHTNESS_MODE,
                    Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL
                )
                Settings.System.putInt(
                    requireContext().contentResolver,
                    Settings.System.SCREEN_BRIGHTNESS,
                    (50 * 2.55).roundToInt()
                )
            }
            100 -> {
                Settings.System.putInt(
                    requireContext().contentResolver,
                    Settings.System.SCREEN_BRIGHTNESS_MODE,
                    Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL
                )
                Settings.System.putInt(
                    requireContext().contentResolver,
                    Settings.System.SCREEN_BRIGHTNESS,
                    (10000 * 2.55).roundToInt()
                )
            }
        }
//        if (modeEntity.brightness == 0) {
//            Settings.System.putInt(
//                requireContext().contentResolver,
//                Settings.System.SCREEN_BRIGHTNESS_MODE,
//                Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC
//            )
//        } else {
//            Settings.System.putInt(
//                requireContext().contentResolver,
//                Settings.System.SCREEN_BRIGHTNESS_MODE,
//                Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL
//            )
//            Settings.System.putInt(
//                requireContext().contentResolver,
//                Settings.System.SCREEN_BRIGHTNESS,
//                (50 * 2.55).roundToInt()
//            )
//        }
        // Set screen orientation
        Settings.System.putInt(
            requireContext().contentResolver,
            Settings.System.ACCELEROMETER_ROTATION,
            if (modeEntity.screenRotation) 1 else 0
        )
        // Set auto sync
        ContentResolver.setMasterSyncAutomatically(modeEntity.autoSync)
        // Set sound
        val audioManager = requireContext().getSystemService(Context.AUDIO_SERVICE) as AudioManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val n =
                requireContext().applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (n.isNotificationPolicyAccessGranted) {
                when (modeEntity.sound) {
                    1 -> audioManager.ringerMode = AudioManager.RINGER_MODE_NORMAL
                    0 -> audioManager.ringerMode = AudioManager.RINGER_MODE_SILENT
                    else -> audioManager.ringerMode = AudioManager.RINGER_MODE_VIBRATE
                }
            }
        } else {
            when (modeEntity.sound) {
                1 -> audioManager.ringerMode = AudioManager.RINGER_MODE_NORMAL
                0 -> audioManager.ringerMode = AudioManager.RINGER_MODE_SILENT
                else -> audioManager.ringerMode = AudioManager.RINGER_MODE_VIBRATE
            }
        }
    }

    private fun sendNotification() {
        if (prefs.getBool(SharedPrefs.SHOW_NOTIFICATIONS)) {
            val pi = PendingIntent.getActivity(
                requireContext(), 0,
                Intent(requireContext(), MainActivity::class.java),
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
                else
                    PendingIntent.FLAG_UPDATE_CURRENT
            )
            val channelId = getString(R.string.app_name)
            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(requireContext(), channelId)
                .setSmallIcon(R.drawable.ic_logo_trial_after)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.tappp))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pi)

            val notificationManager =
                requireContext().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                    channelId,
                    getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_DEFAULT
                )
                notificationManager.createNotificationChannel(channel)
            }

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.home_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_select_all -> {

                if (appsAdapter.selectedAppEntities.size >= appsAdapter.currentList.size){
                    appsAdapter.makeAllUnselected()
                } else {
                    appsAdapter.makeAllSelected()
                }
                true
            }
            R.id.action_sort -> {
                showSortDialog(currentSortType) {
                    currentSortType = it
                    appsAdapter.changeSortType(it)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun updateDeleteView(size: Int) {
        with(binding) {
            if (size == 0) {
                llApplyMode.gone()
                llDelete.gone()
                llAddApps.visible()
            } else {
                llAddApps.gone()
                llApplyMode.visible()
                llDelete.visible()
            }
        }
    }

    private fun showLimitedOffer() {
        val showCount = prefs.getInt(SharedPrefs.OFFER_SHOW_COUNTER)
        val now = Date().time
        val oneDay = 24 * 60 * 60 * 1000.toLong()
        try {
            val installTime = requireContext().packageManager.getPackageInfo(
                requireContext().packageName,
                PackageManager.GET_PERMISSIONS
            ).firstInstallTime

            val lastShow = prefs.getLong(SharedPrefs.LAST_OFFER_SHOW)

            if (!DateUtils.isToday(lastShow) &&
                (showCount == 0 && now - installTime > 2 * oneDay))
            {
                prefs.putValue(SharedPrefs.LAST_OFFER_SHOW, System.currentTimeMillis())
                navToOffer(showCount)
            }

        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

    private fun navToOffer(showCount: Int) {
        val navOptions = NavOptions.Builder().apply {
            setEnterAnim(android.R.anim.fade_in)
            setExitAnim(android.R.anim.fade_out)
        }
        findNavController().navigate(R.id.offerFragment, null, navOptions.build())
        prefs.putValue(SharedPrefs.OFFER_SHOW_COUNTER, showCount + 1)
    }

    override fun onResume() {
        super.onResume()
        if (!requireContext().checkPermissions()) {
            permissionDialog = showPermissionDialog()
        } else
            permissionDialog?.dismiss()
    }

    override fun onPause() {
        super.onPause()
        permissionDialog?.dismiss()
    }
}