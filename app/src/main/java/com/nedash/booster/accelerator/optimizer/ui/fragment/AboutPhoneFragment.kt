package com.nedash.booster.accelerator.optimizer.ui.fragment

import android.app.ActivityManager
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StatFs
import android.text.format.Formatter
import android.util.DisplayMetrics
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nedash.booster.accelerator.optimizer.R
import com.nedash.booster.accelerator.optimizer.databinding.FragmentAboutPhoneBinding
import com.nedash.booster.accelerator.optimizer.utils.Utils.getColoredText
import kotlin.math.pow
import kotlin.math.sqrt


class AboutPhoneFragment : Fragment() {

    private lateinit var binding: FragmentAboutPhoneBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentAboutPhoneBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            tvStorage.text =
                getColoredText(R.string.storage, getTotalInternalMemorySize())
            tvRam.text = getColoredText(R.string.ram, getRam())
            tvScreen.text = getColoredText(R.string.screen, getScreenInches())
            tvAndroid.text = getColoredText(R.string.android, Build.VERSION.RELEASE)
            tvBrand.text = getColoredText(R.string.brand, Build.BRAND)
            tvPosition.text = getColoredText(R.string.position, getOrientation())
            tvTouch.text = getColoredText(R.string.touch, getTouchInput())
            tvManufacture.text = getColoredText(R.string.manufacture, Build.MANUFACTURER)
        }
    }

    private fun getTotalInternalMemorySize(): String {
        val stat = StatFs(Environment.getDataDirectory().path)
        return Formatter.formatFileSize(requireContext(), stat.blockCountLong * stat.blockSizeLong)
    }

    private fun getRam(): String {
        val newActivityManager =
            requireContext().getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val memoryInfo = ActivityManager.MemoryInfo()
        newActivityManager.getMemoryInfo(memoryInfo)
        return Formatter.formatFileSize(requireContext(), memoryInfo.totalMem)
    }

    private fun getScreenInches(): String {
        val dm = DisplayMetrics()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val display = requireActivity().display
            display?.getRealMetrics(dm)
        } else {
            val display = requireActivity().windowManager.defaultDisplay
            display.getMetrics(dm)
        }
        val x = (dm.widthPixels / dm.xdpi).toDouble().pow(2.0)
        val y = (dm.heightPixels / dm.ydpi).toDouble().pow(2.0)
        return String.format("%.1f", sqrt(x + y))
    }

    private fun getTouchInput(): String {
        return when (resources.configuration.touchscreen) {
            Configuration.TOUCHSCREEN_UNDEFINED -> getString(R.string.undefined)
            Configuration.TOUCHSCREEN_NOTOUCH -> getString(R.string.none)
            Configuration.TOUCHSCREEN_FINGER -> getString(R.string.finger)
            else -> getString(R.string.undefined)
        }
    }

    private fun getOrientation(): String {
        return when (resources.configuration.orientation) {
            Configuration.ORIENTATION_UNDEFINED -> getString(R.string.undefined)
            Configuration.ORIENTATION_LANDSCAPE -> getString(R.string.landscape)
            Configuration.ORIENTATION_PORTRAIT -> getString(R.string.portrait)
            else -> getString(R.string.undefined)
        }
    }
}