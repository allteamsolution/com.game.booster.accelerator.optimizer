package com.nedash.booster.accelerator.optimizer.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nedash.booster.accelerator.optimizer.R
import com.nedash.booster.accelerator.optimizer.databinding.ModeSelectorListItemBinding
import com.nedash.booster.accelerator.optimizer.utils.ModeSelector


class ModeSelectorAdapter :
    ListAdapter<ModeSelector, ModeSelectorAdapter.ModeSelectorViewHolder>(
        object : DiffUtil.ItemCallback<ModeSelector>() {
            override fun areItemsTheSame(oldItem: ModeSelector, newItem: ModeSelector) = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: ModeSelector, newItem: ModeSelector) = oldItem == newItem
        }
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModeSelectorViewHolder {
        return ModeSelectorViewHolder(
            ModeSelectorListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ModeSelectorViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class ModeSelectorViewHolder(private val binding: ModeSelectorListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(mode: ModeSelector) {
            with(binding) {
                ivModeIcon.setImageResource(mode.icon)
                if (mode.isSelected) {
                    root.setBackgroundResource(R.drawable.mode_selected_bg)
                    ivModeIcon.imageTintList = ContextCompat.getColorStateList(root.context, R.color.main_green)
                } else {
                    root.setBackgroundResource(R.drawable.mode_unselected_bg)
                    ivModeIcon.imageTintList = ContextCompat.getColorStateList(root.context, R.color.color_A6A6A6)
                }
                root.setOnClickListener {
                    submitList(currentList.map { m ->
                        m.isSelected = m.id == mode.id
                        return@map m
                    })
                    notifyDataSetChanged()
                }
            }
        }
    }
}