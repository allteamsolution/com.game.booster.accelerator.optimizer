package com.nedash.booster.accelerator.optimizer.utils

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.nedash.booster.accelerator.optimizer.R


sealed class SortType(@StringRes val name: Int) {
    object Name : SortType(R.string.name)
    object InstallationTime : SortType(R.string.installation_time)
}

sealed class Setting(
    @DrawableRes val icon: Int,
    @StringRes val title: Int,
    val hasSwitch: Boolean = false
) {
    object RemoveAds : Setting(R.drawable.ic_remove_ads, R.string.remove_ads)
    object Notifications : Setting(R.drawable.ic_notification, R.string.notification, true)
    object AboutYourPhone : Setting(R.drawable.ic_about_phone, R.string.about_your_phone)
    object TermsOfUse : Setting(R.drawable.ic_terms_of_use, R.string.terms_of_use)
    object PrivacyPolicy : Setting(R.drawable.ic_privacy_policy, R.string.privacy_policy)
    object OurSite : Setting(R.drawable.ic_our_site, R.string.our_site)

    companion object {
        fun getAllSettings(isPro: Boolean): List<Setting> {
            val l = arrayListOf(Notifications, AboutYourPhone, TermsOfUse, PrivacyPolicy, OurSite)
            if (!isPro)
                l.add(0, RemoveAds)
            return l
        }
    }
}

sealed class ModeSelector(
    val id: Int,
    @DrawableRes val icon: Int,
    var isSelected: Boolean = false
) {
    object OfflinePlay : ModeSelector(1, R.drawable.ic_no_offline_play)
    object PowerPlay : ModeSelector(2, R.drawable.ic_power_play)
    object LongPlay : ModeSelector(3, R.drawable.ic_long_play)
    object RocketPlay : ModeSelector(4, R.drawable.ic_boost_play)
    object DropPlay : ModeSelector(5, R.drawable.ic_water_drop_play)
    object FlashPlay : ModeSelector(6, R.drawable.ic_light_play)
    object SmartPlay : ModeSelector(7, R.drawable.ic_alarm_off_play)

    companion object {
        fun getAllModes() = listOf(
            OfflinePlay, PowerPlay, LongPlay, RocketPlay, DropPlay, FlashPlay, SmartPlay
        )
    }
}
