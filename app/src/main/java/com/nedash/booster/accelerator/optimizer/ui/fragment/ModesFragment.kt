package com.nedash.booster.accelerator.optimizer.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.nedash.booster.accelerator.optimizer.databinding.FragmentModesBinding
import com.nedash.booster.accelerator.optimizer.db.AppDatabase
import com.nedash.booster.accelerator.optimizer.db.ModeDao
import com.nedash.booster.accelerator.optimizer.db.ModeEntity
import com.nedash.booster.accelerator.optimizer.ui.adapters.ModeAdapter
import com.nedash.booster.accelerator.optimizer.utils.Utils.gone
import com.nedash.booster.accelerator.optimizer.utils.Utils.visible
import kotlinx.coroutines.launch

class ModesFragment : Fragment() {

    private lateinit var binding: FragmentModesBinding
    private lateinit var modeDao: ModeDao
    private val modesAdapter = ModeAdapter(onModeClickListener = {
        findNavController().navigate(
            ModesFragmentDirections.actionModesFragmentToModeSettingsFragment(
                it
            )
        )
    }, onChangeSelectedList = { updateDeleteView(it)
//        val bind = DialogDeleteModeBinding.inflate(layoutInflater)
//        with(Dialog(requireContext())) {
//            requestWindowFeature(Window.FEATURE_NO_TITLE)
//            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            setContentView(bind.root)
//            with(bind) {
//                ivClose.setOnClickListener { dismiss() }
//                btnCancel.setOnClickListener { dismiss() }
//                btnOk.setOnClickListener {
//                    lifecycleScope.launch { modeDao.deleteModes(mode) }
//                    dismiss()
//                }
//            }
//            show()
//        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        modeDao = AppDatabase.provideDatabase(requireContext()).modeDao()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentModesBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            modesAdapter.selectedModesList.clear()
            with(rvItems) {
                layoutManager = GridLayoutManager(requireContext(), 3)
                adapter = modesAdapter
            }
            llAddMode.setOnClickListener {
                findNavController().navigate(
                    ModesFragmentDirections.actionModesFragmentToModeSettingsFragment(ModeEntity())
                )
            }
            llDelete.setOnClickListener {
                lifecycleScope.launch { modeDao.deleteModes(modesAdapter.selectedModesList) }.invokeOnCompletion {
                    with(modesAdapter){
                        makeAllUnselected()
                    }
                }
            }
        }
        modeDao.getAll().observe(viewLifecycleOwner, { modesAdapter.submitList(it) })
    }

    private fun updateDeleteView(size: Int) {
        with(binding) {
            if (size == 0) {
                llDelete.gone()
                llAddMode.visible()
            } else {
                llAddMode.gone()
                llDelete.visible()
            }
        }
    }
}