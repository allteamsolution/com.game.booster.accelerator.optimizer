package com.nedash.booster.accelerator.optimizer.ui.fragment.purchase

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.nedash.booster.accelerator.optimizer.R
import com.nedash.booster.accelerator.optimizer.databinding.FragmentAfterTutorialBinding
import com.nedash.booster.accelerator.optimizer.ui.activity.MainActivity
import com.nedash.booster.accelerator.optimizer.utils.Constants.PP_URL
import com.nedash.booster.accelerator.optimizer.utils.Constants.PURCHASE_CANCEL_SITE
import com.nedash.booster.accelerator.optimizer.utils.Constants.SUBS_YEAR_AFTER
import com.nedash.booster.accelerator.optimizer.utils.Constants.TOU_URL
import com.nedash.booster.accelerator.optimizer.utils.Utils.convertPrice
import com.nedash.booster.accelerator.optimizer.utils.Utils.getColoredText
import com.nedash.booster.accelerator.optimizer.utils.Utils.openURL
import io.github.armcha.autolink.AutoLinkTextView
import io.github.armcha.autolink.MODE_URL

class AfterTutorialFragment : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentAfterTutorialBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.AppBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentAfterTutorialBinding.inflate(layoutInflater, container, false)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            ivClose.setOnClickListener { navNext() }
            with((requireActivity() as MainActivity).billing) {
                isPro.observe(viewLifecycleOwner) {
                    if (it == true)
                        navNext()
                }

                querySubSku(listOf(SUBS_YEAR_AFTER)) {
                    val skuDetails = it.firstOrNull()
                    if (skuDetails != null) {
                        requireActivity().runOnUiThread {
                            with(binding) {
                                tvPerYear.text =
                                    getColoredText(
                                        R.string.per_year_after_trial_ends,
                                        skuDetails.convertPrice()
                                    )
                                createLinkString(
                                    tvBottomHint, getString(
                                        R.string.trial_purchase_bottom_hint,
                                        skuDetails.convertPrice(),
                                        PP_URL,
                                        TOU_URL,
                                        PURCHASE_CANCEL_SITE
                                    )
                                )
                                btnBuy.setOnClickListener {
                                    launchBillingFlow(
                                        requireActivity(),
                                        skuDetails
                                    )
                                }
                            }
                        }
                    } else {
                        navNext()
                    }
                }
            }
        }
        setupBackPressListener()
    }

    private fun createLinkString(textView: AutoLinkTextView, dataText: String) {
        with(textView) {
            addAutoLinkMode(MODE_URL)
            attachUrlProcessor { s: String ->
                when {
                    s.equals(TOU_URL, ignoreCase = true) -> {
                        return@attachUrlProcessor "Terms and Conditions"
                    }
                    s.equals(PP_URL, ignoreCase = true) -> {
                        return@attachUrlProcessor "Privacy Policy"
                    }
                    else -> {
                        return@attachUrlProcessor "Cancel"
                    }
                }
            }
            addSpan(MODE_URL, StyleSpan(Typeface.NORMAL), UnderlineSpan())
            urlModeColor = currentTextColor
            text = dataText
            onAutoLinkClick { (_, _, originalText) -> requireContext().openURL(originalText) }
        }
    }

    private fun setupBackPressListener() {
        this.view?.isFocusableInTouchMode = true
        this.view?.requestFocus()
        this.view?.setOnKeyListener { _, keyCode, _ ->
            keyCode == KeyEvent.KEYCODE_BACK
        }
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        navNext()
    }

    private fun navNext() {
        findNavController().navigate(AfterTutorialFragmentDirections.actionAfterTutorialFragmentToHomeFragment())
    }
}