package com.nedash.booster.accelerator.optimizer.ui.fragment.purchase

import android.graphics.Paint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.nedash.booster.accelerator.optimizer.R
import com.nedash.booster.accelerator.optimizer.databinding.FragmentOfferBinding
import com.nedash.booster.accelerator.optimizer.ui.activity.MainActivity
import com.nedash.booster.accelerator.optimizer.utils.Constants.SUBS_OFFER
import com.nedash.booster.accelerator.optimizer.utils.Utils.calculateOldPrice
import com.nedash.booster.accelerator.optimizer.utils.Utils.calculatePeriodPrice
import com.nedash.booster.accelerator.optimizer.utils.Utils.convertPrice
import com.nedash.booster.accelerator.optimizer.utils.Utils.visible

class OfferFragment : Fragment() {

    private lateinit var binding: FragmentOfferBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentOfferBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            llClose.setOnClickListener { findNavController().popBackStack() }
            with((requireActivity() as MainActivity).billing) {
                isPro.observe(
                    viewLifecycleOwner,
                    { if (it == true) findNavController().popBackStack() })
                querySubSku(listOf(SUBS_OFFER)) {
                    val skuDetails = it.firstOrNull()
                    skuDetails?.let { sku ->
                        requireActivity().runOnUiThread {
                            tvWeekly.text =
                                getString(
                                    R.string.price_weekly,
                                    sku.calculatePeriodPrice(52)
                                )
                            tvAnnually.text = getString(R.string.price_annually, sku.convertPrice())
                            with(tvEveryAnnually) {
                                paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                                text = getString(
                                    R.string.price_annually,
                                    sku.calculateOldPrice(50)
                                )
                            }
                            btnBuy.setOnClickListener { launchBillingFlow(requireActivity(), sku) }
                            clContent.visible()
                        }
                    }
                }
            }
        }
    }
}