package com.nedash.booster.accelerator.optimizer.ui.adapters

import android.app.Dialog
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nedash.booster.accelerator.optimizer.databinding.SelectModeListItemBinding
import com.nedash.booster.accelerator.optimizer.db.ModeEntity

class SelectModeAdapter(
    private val items: List<ModeEntity>,
    var selectedMode: Int,
    val dialog: Dialog,
    val onItemClicked: (Int) -> Unit
) : RecyclerView.Adapter<SelectModeAdapter.SelectModeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectModeViewHolder {
        return SelectModeViewHolder(
            SelectModeListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: SelectModeViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    inner class SelectModeViewHolder(private val binding: SelectModeListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(modeEntity: ModeEntity) {
            with(binding) {
                ivModeIcon.setImageResource(modeEntity.getIcon())
                tvTitle.text = modeEntity.name
                with(root) {
                    setOnClickListener {
                        selectedMode = modeEntity.id
                        notifyDataSetChanged()
                        onItemClicked.invoke(selectedMode)

                        dialog.dismiss()
                    }
                }

            }
        }
    }
}